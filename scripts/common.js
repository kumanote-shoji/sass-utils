const path = require('path');

const fsp = require('fs-promise');
const sass = require('node-sass');
const globby = require('globby');
const notifier = require('node-notifier');

const baseDir = path.dirname(__dirname);
const exampleDir = path.join(baseDir, 'example');
const libraryDir = path.join(baseDir, 'lib');

const processor = require('postcss')([
  require('autoprefixer')({
    browsers: ['> 5%', 'last 2 versions', 'Android 2.2', 'iOS 4', 'ie 9', 'Safari 5']
  }),
  require('css-mqpacker')()]);

function thenableSass(filePath) {
  return new Promise((resolve, reject) => {
    sass.render({
      file: filePath
    }, (err, result) => {
      if (err) {
        reject(err);
        return;
      }
      processor.process(result.css).then((result) => {
        resolve(result.css);
      }).catch((er) => {
        reject(er);
      });
    });
  });
}

module.exports = {
  isExample: (filePath) => {
    return !/^\.\./.test(path.relative(exampleDir, filePath));
  },

  isLibrary: (filePath) => {
    return !/^\.\./.test(path.relative(libraryDir, filePath));
  },

  getExamplePattern: () => {
    return path.join(exampleDir, '**', '*.{scss,sass}');
  },

  getLibraryPattern: () => {
    return path.join(libraryDir, '**', '*.{scss,sass}');
  },

  getExample: () => {
    return globby(module.exports.getExamplePattern());
  },

  getLibrary: () => {
    return globby(module.exports.getLibraryPattern());
  },

  build: (filePath) => {
    const outPath = filePath.replace(/\.(sass|scss)$/, '.css');
    return thenableSass(filePath).then((buffer) => {
      return fsp.writeFile(outPath, buffer);
    }).then(() => {
      console.log(`write  : ${path.relative(baseDir, outPath)}`);
    }).catch((er) => {
      console.log();
      console.error(er.formatted || er);
      console.log();
      notifier.notify(er.formatted || er);
    });
  },

  clean: (filePath) => {
    const outPath = filePath.replace(/\.(sass|scss)$/, '.css');
    fsp.unlink(outPath).then(() => {
      console.log(`delete : ${path.relative(baseDir, outPath)}`);
    });
  }
};
