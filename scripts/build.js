const common = require('./common');

common.getExample().then((paths) => {
  return Promise.all(paths.map((path) => {
    return common.build(path);
  }));
}).then(() => {
  console.log('done');
});
