const chokidar = require('chokidar');
const common = require('./common');

function handler(path) {
  if (common.isExample(path)) {
    common.build(path);
  } else if (common.isLibrary(path)) {
    common.getExample().then((paths) => {
      paths.forEach((path) => common.build(path));
    });
  }
}

const watcher = chokidar.watch([
  common.getExamplePattern(),
  common.getLibraryPattern()
]).on('ready', () => {
  console.log('start watch');
  watcher.
    on('add', handler).
    on('change', handler).
    on('unlink', (path) => {
      if (common.isExample(path)) {
        common.clean(path);
      }
    });
});
