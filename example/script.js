(function() {
  'use strict';

  function setFont() {
    [].forEach.call(document.querySelectorAll('.font-jp'), function(element) {
      element.setAttribute('data-label', window.getComputedStyle(element, null).fontFamily);
    });
  }

  setFont();
})();
